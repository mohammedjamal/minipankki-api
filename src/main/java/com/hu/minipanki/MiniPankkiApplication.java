package com.hu.minipanki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniPankkiApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(MiniPankkiApplication.class, args);
		
		Send send = new Send();
		send.main(args);
	}

}
