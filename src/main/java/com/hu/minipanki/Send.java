package com.hu.minipanki;

import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Send {
	
	public final static String QUEUE_NAME = "hello";
	private static Logger logger = LoggerFactory.getLogger(Send.class);
	
	public static void main(String[] args) throws Exception {
		ConnectionFactory connectionFactory = new ConnectionFactory();
		connectionFactory.setHost("localhost");
		
		Connection connection = connectionFactory.newConnection();
		Channel channel = connection.createChannel();
		
		
		try(connection; channel) {
			channel.queueDeclare(QUEUE_NAME, false, false, false, null);
			
			for(int i=0; i<=100;i++) {
				String message = "Hello My Rabbit " + i;
				Thread.sleep(1000);
				channel.basicPublish("", QUEUE_NAME, null,  message.getBytes(StandardCharsets.UTF_8));
				// System.out.println(" [x] Sent '" + message +"'");
				logger.info(" [x] Sending '" + message +"' to Queue " + QUEUE_NAME);

			}
			
						
		}
	}
}
